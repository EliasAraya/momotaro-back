import { check } from "express-validator";
import { validateResults } from "../utils/handleValidator.js";

export const validatorPedido = [
  check("id_usuario").notEmpty().withMessage("El campo 'id_usuario' es requerido"),
  check("fecha").notEmpty().withMessage("El campo 'fecha' es requerido"),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];
