import { check } from "express-validator";
import { validateResults } from "../utils/handleValidator.js";

export const valitadorReserva = [
  check("id_usuario").notEmpty().withMessage("El campo 'id_usuario' es requerido"),
  check("id_mesa").notEmpty().withMessage("El campo 'id_mesa' es requerido"),
  check("fecha").notEmpty().withMessage("El campo 'fecha' es requerido"),
  check("hora").notEmpty().withMessage("El campo 'hora' es requerido"),
  check("cantidad").notEmpty().withMessage("El campo 'cantidad' es requerido"),
  check("mensaje"),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];
export const valitadorReservaGeneral = [
  check("nombre").notEmpty().withMessage("El campo 'id_usuario' es requerido"),
  check("email").notEmpty().withMessage("El campo 'id_usuario' es requerido"),
  check("id_mesa").notEmpty().withMessage("El campo 'id_mesa' es requerido"),
  check("fecha").notEmpty().withMessage("El campo 'fecha' es requerido"),
  check("hora").notEmpty().withMessage("El campo 'hora' es requerido"),
  check("telefono"),
  check("cantidad").notEmpty().withMessage("El campo 'cantidad' es requerido"),
  check("mensaje"),

  (req, res, next) => {
    return validateResults(req, res, next);
  },
];
