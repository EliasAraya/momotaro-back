import { check } from "express-validator";
import { validateResults } from "../utils/handleValidator.js";

export const validatorMesa = [
  check("numero").notEmpty().withMessage("El campo 'numero' es requerido"),
  check("capacidad").notEmpty().withMessage("El campo 'capacidad' es requerido"),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];
