import { check } from "express-validator";
import { validateResults } from "../utils/handleValidator.js";

export const validatorRegister = [
  check("nombre").notEmpty().withMessage("El campo 'nombre' es requerido"),
  check("email").notEmpty().withMessage("El campo 'email' es requerido"),
  check("direccion").notEmpty().withMessage("El campo 'direccion' es requerido"),
  check("password")
    .notEmpty()
    .withMessage("El campo 'password' es requerido")
    .isLength({ min: 6, max: 99 })
    .withMessage("La contraseña debe tener entre 6 y 99 caracteres"),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];

export const validatorLogin = [
  check("email").notEmpty().withMessage("El campo 'email' es requerido"),
  check("password")
    .notEmpty()
    .withMessage("El campo 'password' es requerido")
    .isLength({ min: 6, max: 99 })
    .withMessage("La contraseña debe tener entre 6 y 99 caracteres"),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];
