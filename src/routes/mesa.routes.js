import { Router } from "express";
import { listMesa, createMesa, patchMesa, deleteMesa } from "../controllers/mesa.controller.js";
import { authMiddleware } from "../middleware/session.js";
import { checkRol } from "../middleware/rol.js";
import { validatorMesa } from "../validators/mesa.validators.js";

const router = Router();

router.get("/listMesa", authMiddleware, listMesa);
router.post("/crearMesa", authMiddleware, checkRol(["administrador"]), validatorMesa, createMesa);
router.patch("/patchMesa/:id", authMiddleware, checkRol(["administrador"]), patchMesa);
router.delete("/eliminarMesa/:id", authMiddleware, checkRol(["administrador"]), deleteMesa);

export default router;
