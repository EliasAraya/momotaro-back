import { Router } from "express";
import { authMiddleware } from "../middleware/session.js";
import {
  getProducto,
  getProductoById,
  updateProducto,
  postProducto,
  postMasivoProducto,
  delProducto,
  addStockProducto,
} from "../controllers/producto.controller.js";
import { checkRol } from "../middleware/rol.js";
/* import { upload } from "../utils/multer.config.js"; */
import { upload } from "../middleware/multer.middleware.js";

const router = Router();

router.get("/producto", getProducto);
router.get("/producto/:id", authMiddleware, getProductoById);
router.patch("/producto/:sku", authMiddleware, checkRol(["administrador"]), updateProducto);
router.put("/producto/:sku", authMiddleware, checkRol(["administrador"]), addStockProducto);
router.delete("/producto/:sku", authMiddleware, checkRol(["administrador"]), delProducto);
router.post("/producto", authMiddleware, checkRol(["administrador"]), postProducto);
router.post(
  "/cargaMasivaProducto",
  authMiddleware,
  checkRol(["administrador"]),
  upload.single("file"),
  postMasivoProducto
);

export default router;
