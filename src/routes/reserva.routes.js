import { Router } from "express";
import {
  createReserva,
  listReservaByClient,
  listReservas,
  patchReserva,
  delReserva,
  cancelReserva,
  createReservaGeneral,
  cancelReservaGeneral,
} from "../controllers/reserva.controller.js";
import { authMiddleware } from "../middleware/session.js";
import { checkRol } from "../middleware/rol.js";
import { valitadorReserva, valitadorReservaGeneral } from "../validators/reserva.validators.js";

const router = Router();

router.get("/listReservaById/:id", authMiddleware, listReservaByClient);

router.get("/listReservas", authMiddleware, checkRol(["administrador"]), listReservas);

router.post("/crearReserva", authMiddleware, valitadorReserva, createReserva);
router.post("/crearReservaGeneral", valitadorReservaGeneral, createReservaGeneral);
router.patch("/patchReserva/:id", authMiddleware, patchReserva);
router.delete("/eliminarReserva/:id", authMiddleware, delReserva);
router.patch("/cancelarReserva/:id", authMiddleware, cancelReserva);
router.patch("/cancelarReservaG/:id", cancelReservaGeneral);

export default router;
