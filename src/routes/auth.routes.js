import { Router } from "express";
import { validatorRegister, validatorLogin } from "../validators/auth.js";
import {
  signIn,
  logIn,
  logInAdmin,
  logOut,
} from "../controllers/auth.controller.js";

const router = Router();

router.post("/register", validatorRegister, signIn);

router.post("/login", validatorLogin, logIn);

router.post("/loginAdmin", validatorLogin, logInAdmin);

router.post("/logOut", logOut);

export default router;
