import { Router } from "express";
import { authMiddleware } from "../middleware/session.js";
import {
  cancelarPedido,
  listPedido,
  listPedidos,
  createPedido,
  listFactura,
} from "../controllers/pedido.controller.js";
import { checkRol } from "../middleware/rol.js";

const router = Router();

router.get("/listPedido/:id", authMiddleware, listPedido);
router.get("/listFactura/:id", authMiddleware, listFactura);
router.get("/listPedido", authMiddleware, checkRol(["administrador"]), listPedidos);
router.patch("/cancelarPedido/:id", authMiddleware, checkRol(["administrador"]), cancelarPedido);
router.post("/pedido", authMiddleware, createPedido);

export default router;
