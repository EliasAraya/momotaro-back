import { Router } from "express";
import { listUsers } from "../controllers/users.controller.js";
import { authMiddleware } from "../middleware/session.js";
import { checkRol } from "../middleware/rol.js";

const router = Router();

router.get(
  "/listUsers",
  authMiddleware,
  checkRol(["administrador"]),
  listUsers
);

export default router;
