/**
 * Array con los roles permitidos
 * @param {*} req
 */
export const checkRol = (roles) => async (req, res, next) => {
  try {
    const { user } = req;
    const rolesByUser = user.tipo;

    const checkValueRol = roles.some((rolSingle) =>
      rolesByUser.includes(rolSingle)
    );

    if (!checkValueRol) {
      return res
        .status(403)
        .send({ mensaje: "El Usuario no tiene los permisos" });
    }
    next();
  } catch (error) {
    res.status(403).send({ mensaje: "Error con los permisos" });
  }
};
