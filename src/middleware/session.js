import { verifyToken } from "../utils/handleJwt.js";

export const authMiddleware = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      res.status(401).send({ mensaje: "NOT_TOKEN" });
      return;
    }

    const token = req.headers.authorization.split(" ").pop();
    const dataToken = await verifyToken(token);

    if (!dataToken.email) {
      res.status(401).send({ mensaje: "TOKEN INVALIDO" });
      return;
    }

    const user = { email: dataToken.email, tipo: dataToken.tipo };
    req.user = user;
    next();
  } catch (error) {
    const status = 401;
    res.status(401).send({ error, status });
  }
};
