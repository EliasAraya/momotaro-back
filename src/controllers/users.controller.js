import { pool } from "../db.js";

/**
 * Este controlador es el encargado de listar los usuarios
 * @param {*} res
 */
export const listUsers = async (req, res) => {
  try {
    const [rows] = await pool.query("SELECT * FROM Usuario");
    res.send({ data: rows });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
