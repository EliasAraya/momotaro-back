import { pool } from "../db.js";
import fs from "fs";
import fastcsv from "fast-csv";

export const getProducto = async (req, res) => {
  try {
    const [rows] = await pool.query("SELECT * FROM Producto");
    if (rows.length <= 0) {
      return res.status(404).send({ data: { mensaje: "No hay productos registrados", statusCode: 404 } });
    }

    res.send({ data: rows, statusCode: 200 });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const getProductoById = async (req, res) => {
  try {
    const { id } = req.params;
    const [rows] = await pool.query("SELECT * FROM Producto WHERE sku = ?", [id]);
    console.log(id);
    if (rows.length <= 0) {
      return res
        .status(404)
        .send({ data: { mensaje: "No se encontro el producto solicitado.", statusCode: 404 } });
    }

    res.send({ data: rows[0], statusCode: 200 });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const updateProducto = async (req, res) => {
  try {
    const { sku } = req.params;
    const { nombre, descripcion, precio, categoria, stock } = req.body;

    const sqlUpdateProducto = `UPDATE Producto 
      SET nombre = IFNULL(?, nombre), descripcion = IFNULL(?,descripcion), precio = IFNULL(?,precio), categoria = IFNULL(?,categoria), stock = IFNULL(?,stock) WHERE sku = ?`;

    const [rows] = await pool.query(sqlUpdateProducto, [
      nombre,
      descripcion,
      precio,
      categoria,
      stock,
      sku,
    ]);

    console.log(rows);
    if (rows.affectedRows === 0) {
      return res
        .status(404)
        .send({ data: { mensaje: "No se encontro el producto solicitado.", statusCode: 404 } });
    }
    if (rows.changedRows === 0) {
      return res
        .status(400)
        .send({ data: { mensaje: "No se pudo actualizar el producto solicitado.", statusCode: 400 } });
    }

    res.send({
      data: { mensaje: `El producto con Sku:${sku} fue actualizado con exito`, statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const addStockProducto = async (req, res) => {
  try {
    const { sku } = req.params;
    const { stock } = req.body;
    const sqlSelectStock = "SELECT stock FROM Producto WHERE sku = ?";
    const [row] = await pool.query(sqlSelectStock, [sku]);

    if (row.length <= 0) {
      return res.status(404).send({ mensaje: "No se encontro el producto solicitado.", statusCode: 404 });
    }

    const newStock = row[0].stock + stock;

    const sqlUpdateProducto = "UPDATE Producto SET stock = ? WHERE sku = ?";

    const [rows] = await pool.query(sqlUpdateProducto, [newStock, sku]);

    if (rows.affectedRows === 0) {
      return res
        .status(404)
        .send({ data: { mensaje: "No se encontro el producto solicitado.", statusCode: 404 } });
    }
    if (rows.changedRows === 0) {
      return res.status(400).send({
        data: { mensaje: "No se pudo actualizar el stock del producto solicitado.", statusCode: 400 },
      });
    }

    res.send({
      data: { mensaje: `El Stock del producto Sku:${sku} fue actualizado con exito`, statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const delProducto = async (req, res) => {
  try {
    const { sku } = req.params;
    const sqlDelete = "DELETE FROM Producto WHERE sku = ?";
    const [rows] = await pool.query(sqlDelete, [sku]);

    if (rows.affectedRows === 0) {
      return res
        .status(404)
        .send({ data: { mensaje: "No se encontro un producto con ese Sku.", statusCode: 404 } });
    }
    res.send({ data: { mensaje: "Producto eliminado exitosamente.", statusCode: 200 } });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const postProducto = async (req, res) => {
  try {
    const { nombre, descripcion, precio, categoria, stock, sku } = req.body;
    const sqlPost =
      "INSERT INTO Producto (nombre,descripcion,precio,categoria,stock,sku) VALUES (?,?,?,?,?,?)";
    await pool.query(sqlPost, [nombre, descripcion, precio, categoria, stock, sku]);

    res.send({ data: { mensaje: "Producto ingresado exitosamente.", statusCode: 200 } });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const postMasivoProducto = async (req, res) => {
  try {
    const products = [];
    fastcsv
      .parseStream(fs.createReadStream(req.file.path), { delimiter: ";" })
      .on("data", function (row) {
        const product = {
          nombre: row[0],
          descripcion: row[1],
          precio: row[2],
          categoria: row[3],
          stock: row[4],
          sku: row[5],
        };
        products.push(product);
      })
      .on("end", function () {
        products.shift();

        const producto = products.map((item) => [
          item.nombre,
          item.descripcion,
          item.precio,
          item.categoria,
          item.stock,
          item.sku,
        ]);
        const sql =
          "INSERT INTO Producto (nombre, descripcion, precio, categoria, stock, sku) VALUES ?" +
          "ON DUPLICATE KEY UPDATE stock = stock + VALUES(stock)";
        pool.query(sql, [producto]);

        res.send({ data: { mensaje: "Carga masiva completa.", statusCode: 200 } });
      });
  } catch (error) {
    res.status(500).send("Error en el servidor");
  }
};
