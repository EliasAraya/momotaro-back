import { matchedData } from "express-validator";
import { pool } from "../db.js";

export const listMesa = async (req, res) => {
  try {
    const [rows] = await pool.query("SELECT * FROM Mesa WHERE NOT estado = 'ocupada'");

    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "No existen mesas actualmente" });
    }

    res.send({ data: rows, statusCode: 200 });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const createMesa = async (req, res) => {
  try {
    req = matchedData(req);
    const [rows] = await pool.query(
      "INSERT INTO Mesa (numero, capacidad, estado) VALUES (?,?,'disponible')",
      [req.numero, req.capacidad]
    );

    res.send({ data: { mensaje: "Reserva creada exitosamente", statusCode: 200 } });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const patchMesa = async (req, res) => {
  try {
    const { id } = req.params;
    const { numero, capacidad, estado } = req.body;
    const sqlUpdate =
      "UPDATE Mesa SET numero = IFNULL(?,numero), capacidad = IFNULL(?,capacidad), estado = IFNULL(?,estado) WHERE id_mesa = ?";
    const [rows] = await pool.query(sqlUpdate, [numero, capacidad, estado, id]);
    console.log(rows);
    if (rows.affectedRows === 0) {
      return res.status(404).send({ data: { mensaje: "Mesa no encontrada", statusCode: 404 } });
    }

    res.send({
      data: { mensaje: "Mesa actualizada con exito", statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const deleteMesa = async (req, res) => {
  try {
    const { id } = req.params;
    const [result] = await pool.query("DELETE FROM Mesa where id_mesa = ?", [id]);

    if (result.affectedRows === 0) {
      return res.status(404).send({ data: { mensaje: "Mesa no encontrada", statusCode: 404 } });
    }

    res.send({
      data: { mensaje: "Mesa eliminada con exito", statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
