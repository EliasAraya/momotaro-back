import moment from "moment/moment.js";
import pdf from "pdfkit";
import { pool } from "../db.js";
import { createInvoice } from "../utils/handlePdf.js";
import {
  sqlFactura,
  sqlGetPedido,
  sqlGetAllPedido,
  sqlPutPedido,
  sqlDetallePedido,
  sqlStockPedido,
  sqlUpdateStock,
  sqlPostPedido,
  sqlVerificarStock,
  sqlUpdateStockPost,
  sqlPostDetalle,
} from "../utils/querys.js";

export const listPedido = async (req, res) => {
  try {
    const { id } = req.params;
    const [rows] = await pool.query(sqlGetPedido, [id]);

    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "No posees ningun pedido actualmente." });
    }

    const data = rows.map((pedido) => {
      console.log("Pedido ", pedido);
      return {
        id_pedido: pedido.id_pedido,
        fecha: moment(pedido.fecha).format("DD/MM/YYYY"),
        hora: moment(pedido.fecha).format("HH:mm"),
        estado: pedido.estado,
      };
    });
    console.log(data);

    res.send({ data, statusCode: 200 });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const listPedidos = async (req, res) => {
  try {
    const [rows] = await pool.query(sqlGetAllPedido);

    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "No existen pedidos actualmente." });
    }

    const data = rows.map((pedido) => {
      return {
        id_pedido: pedido.id_pedido,
        id_usuario: pedido.id_usuario,
        fecha: moment(pedido.fecha).format("DD/MM/YYYY"),
        hora: moment(pedido.fecha).format("HH:mm"),
        estado: pedido.estado,
      };
    });

    res.send({ data, statusCode: 200 });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const cancelarPedido = async (req, res) => {
  try {
    const { id } = req.params;
    const [rows] = await pool.query(sqlPutPedido, [id]);
    console.log(rows);
    if (rows.affectedRows === 0) {
      return res
        .status(404)
        .send({ data: { mensaje: "No se encontro un pedido con ese Id.", statusCode: 404 } });
    }

    const [rowsDetalle] = await pool.query(sqlDetallePedido, [id]);

    if (rowsDetalle.length <= 0) {
      return res.status(404).send({ mensaje: "No existen un detalle para ese pedido." });
    }

    const producto = {
      sku: rowsDetalle[0].sku,
      cantidad: rowsDetalle[0].cantidad,
    };

    const [rowsProducto] = await pool.query(sqlStockPedido, [producto.sku]);

    const newStock = producto.cantidad + rowsProducto[0].stock;
    await pool.query(sqlUpdateStock, [newStock, sku]);
    res.send({ data: { mensaje: "Pedido cancelado con exito.", statusCode: 200 } });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const createPedido = async (req, res) => {
  try {
    const fechaActual = moment();
    fechaActual.subtract(30, "minutes");
    const { id_usuario, productos, fecha, precio } = req.body;

    const fecha_hora = moment(fecha, "DD/MM/YYYY HH:mm");
    if (fecha_hora.isBefore(fechaActual)) {
      return res.status(403).send({
        mensaje: "El pedido no puede ser previo a la fecha y hora actual",
      });
    }

    const [rows] = await pool.query(sqlPostPedido, [id_usuario, fecha_hora.format("YYYY-MM-DD HH:mm:ss")]);

    const id_pedido = rows.insertId;

    for (const producto of productos) {
      const { sku, cantidad } = producto;

      const [rowsProduct] = await pool.query(sqlVerificarStock, [sku, cantidad]);

      let product = rowsProduct[0];
      if (rowsProduct.length === 0) {
        return res.status(403).send({ mensaje: `El producto con ID ${sku} no tiene suficiente Stock` });
      }
      const newStock = product.stock - cantidad;

      await pool.query(sqlUpdateStockPost, [newStock, sku]);

      const totalDetalle = cantidad * product.precio;
      await pool.query(sqlPostDetalle, [id_pedido, sku, cantidad, totalDetalle]);
    }

    const data = {
      id_pedido,
      mensaje: "Pedido creado exitosamente.",
      statusCode: 200,
    };

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const listFactura = async (req, res) => {
  try {
    const { id } = req.params;
    let subtotal = 0;
    const [products] = await pool.query(sqlFactura, [id]);
    const producto = products[0];

    const invoice = {
      usuario: {
        nombre: producto.nombre_usuario,
        calle: producto.direccion.split(",")[0],
        comuna: producto.direccion.split(",")[1],
        ciudad: producto.direccion.split(",")[2],
      },
      productos: [],
      pedido: {
        id_pedido: producto.id_pedido,
        fecha: moment(producto.fecha).format("DD/MM/YYYY HH:mm"),
      },
      detalle: {
        cantidad: producto.cantidad,
        total: producto.total,
      },
      subtotal: subtotal,
    };
    invoice.productos = products.map((item) => {
      invoice.subtotal += item.total;
      return {
        nombre: item.nombre,
        descripcion: item.descripcion,
        precio: item.precio,
        cantidad: item.cantidad,
        total: item.total,
      };
    });

    /* invoice.subtotal = subtotal; */
    const doc = await createInvoice(invoice);

    // establece el encabezado 'Content-Type' para el tipo de archivo que estás enviando
    res.setHeader("Content-Type", "application/pdf");

    // si quieres que el archivo se descargue automáticamente con un nombre específico, puedes establecer el encabezado 'Content-Disposition'
    res.setHeader("Content-Disposition", "attachment; filename=invoice.pdf");

    // pipe el stream del PDF a la respuesta
    doc.pipe(res);
    doc.end();
    /* res.send({ mensaje: "HECHO", data: products }); */
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
