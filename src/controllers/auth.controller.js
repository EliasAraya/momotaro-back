import { pool } from "../db.js";
import { matchedData } from "express-validator";
import { encrypt, compare } from "../utils/handlePassword.js";
import { tokenSign } from "../utils/handleJwt.js";

export const signIn = async (req, res) => {
  try {
    req = matchedData(req);
    const tipo = "cliente";
    const statusCode = 200;
    const password = await encrypt(req.password);
    console.log(req);
    await pool.execute(
      "INSERT INTO Usuario (nombre_usuario,email,password,tipo, direccion) VALUES (?,?,?,?,?)",
      [req.nombre, req.email, password, tipo, req.direccion]
    );

    const data = {
      token: await tokenSign({ email: req.email, tipo }),
      message: `El usuario ${req.nombre} fue creado con exito`,
      statusCode,
    };
    res.send({ data });
  } catch (error) {
    console.error("Error al insertar los datos:", error);
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

/**
 * Este controlador es el encargado de loguear una persona
 * @param {*} res
 */
export const logIn = async (req, res) => {
  try {
    console.log("Api");
    req = matchedData(req);
    const [rows] = await pool.query("SELECT * FROM Usuario WHERE email = ?", [req.email]);
    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "Usuario no encontrado" });
    }

    const user = rows[0];

    const hashPassword = user.password;
    const check = await compare(req.password, hashPassword);

    if (!check) {
      return res.status(401).send({ mensaje: "Contraseña invalida " });
    }

    const data = {
      token: await tokenSign({ email: user.email, tipo: user.tipo }),
      user,
    };
    //Opcion para borrar Password de la respuesta
    //Pendiente de verificacion

    delete data.user.password;

    res.status(200).send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
/**
 * Este controlador es el encargado de loguear una persona
 * @param {*} res
 */
export const logInAdmin = async (req, res) => {
  try {
    req = matchedData(req);
    const [rows] = await pool.query("SELECT * FROM Usuario WHERE email = ?", [req.email]);
    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "Usuario no encontrado" });
    }

    const user = rows[0];

    const hashPassword = user.password;
    const check = await compare(req.password, hashPassword);

    if (!check) {
      return res.status(401).send({ mensaje: "Contraseña invalida " });
    }

    if (user.tipo !== "admin") {
      return res.status(403).send({ mensaje: "Acceso denegado" });
    }

    const data = {
      token: await tokenSign({ email: user.email, tipo: user.tipo }),
      user,
    };
    //Opcion para borrar Password de la respuesta
    //Pendiente de verificacion

    delete data.user.password;

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
/**
 * Este controlador es el encargado de desloguear una persona
 * @param {*} res
 */
export const logOut = async (req, res) => {
  try {
    const token = req.headers.authorization?.split(" ")[1];
    await pool.query("INSERT INTO Token (black_list) VALUES (?)", [token]);

    res.send({ mensaje: "Logout exitoso" });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
