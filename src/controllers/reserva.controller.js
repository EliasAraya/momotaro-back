import { matchedData } from "express-validator";
import { pool } from "../db.js";
import moment from "moment/moment.js";
import { query } from "express";

/**
 * Este controlador es el encargado de listar los usuarios
 * @param {*} res
 */
export const listReservaByClient = async (req, res) => {
  try {
    const fechaActual = moment();
    const [rows] = await pool.query(
      "SELECT * FROM Reserva where id_usuario = ? AND fecha_hora >= ? AND NOT estado = 'cancelada'",
      [req.params.id, fechaActual.subtract(15, "minutes").format("YYYY-MM-DD HH:mm:ss")]
    );

    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "El usuario no tiene reservas" });
    }
    const user = rows[0];
    const data = {
      id_reserva: user.id_reserva,
      id_mesa: user.id_mesa,
      fecha: moment(user.fecha).format("DD-MM-YYYY"),
      hora: moment(user.fecha_hora).format("HH:mm"),
      estado: user.estado,
    };

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const listReservas = async (req, res) => {
  try {
    const fechaActual = moment();
    const [rows] = await pool.query(
      "SELECT * FROM Reserva where fecha_hora >= ? AND NOT estado = 'cancelada'",
      [fechaActual.subtract(15, "minutes").format("YYYY-MM-DD HH:mm:ss")]
    );

    if (rows.length <= 0) {
      return res.status(404).send({ mensaje: "No existen reservas actualmente" });
    }

    const data = rows.map((user) => {
      return {
        id_reserva: user.id_reserva,
        id_usuario: user.id_usuario,
        id_mesa: user.id_mesa,
        fecha: moment(user.fecha_hora).format("DD-MM-YYYY"),
        hora: moment(user.fecha_hora).format("HH:mm"),
        estado: user.estado,
      };
    });

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const createReserva = async (req, res) => {
  try {
    const fechaActual = moment();
    req = matchedData(req);
    const fecha_hora = moment(`${req.fecha} ${req.hora}`, "DD-MM-YYYY HH:mm");
    if (fecha_hora.isBefore(fechaActual)) {
      return res.status(403).send({
        mensaje: "La reserva no puede ser previo a la fecha y hora actual",
      });
    }

    const sqlList =
      "SELECT * FROM Reserva where id_usuario = ? AND fecha_hora >= ? AND NOT estado = 'cancelada'";
    const [rowList] = await pool.execute(sqlList, [
      req.id_usuario,
      fechaActual.subtract(15, "minutes").format("YYYY-MM-DD HH:mm:ss"),
    ]);

    if (rowList.length > 0) {
      return res.send({
        mensaje: `Ya existe una reserva creada con estado ${rowList[0].estado}`,
      });
    }

    const estado = "pendiente";
    const sql =
      "INSERT INTO Reserva (id_usuario,id_mesa,fecha_hora,cantidad,mensaje,estado) VALUES (?,?,?,?,?,?)";
    const [rows] = await pool.execute(sql, [
      req.id_usuario,
      req.id_mesa,
      fecha_hora.format("YYYY-MM-DD HH:mm:ss"),
      req.cantidad,
      req.mensaje,
      estado,
    ]);

    const sqlUpdate = "UPDATE Mesa SET estado = 'ocupada' WHERE id_mesa = ?";
    await pool.query(sqlUpdate, [req.id_mesa]);

    const user = rows[0];
    const data = {
      ...user,
      mensaje: "Reserva creada exitosamente",
    };

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const createReservaGeneral = async (req, res) => {
  try {
    const fechaActual = moment();
    req = matchedData(req);
    const fecha_hora = moment(`${req.fecha} ${req.hora}`, "DD-MM-YYYY HH:mm");
    if (fecha_hora.isBefore(fechaActual)) {
      return res.status(403).send({
        mensaje: "La reserva no puede ser previo a la fecha y hora actual",
      });
    }

    const sqlList =
      "SELECT * FROM ReservaGeneral where email = ? AND fecha_hora >= ? AND NOT estado = 'cancelada'";
    const [rowList] = await pool.execute(sqlList, [
      req.email,
      fechaActual.subtract(15, "minutes").format("YYYY-MM-DD HH:mm:ss"),
    ]);

    if (rowList.length > 0) {
      return res.send({
        mensaje: `Ya existe una reserva creada con estado ${rowList[0].estado}`,
      });
    }

    const estado = "pendiente";
    const sql =
      "INSERT INTO ReservaGeneral (id_mesa,nombre,email,telefono,fecha_hora,cantidad,mensaje,estado) VALUES (?,?,?,?,?,?,?,?)";
    const [rows] = await pool.execute(sql, [
      req.id_mesa,
      req.nombre,
      req.email,
      req.telefono,
      fecha_hora.format("YYYY-MM-DD HH:mm:ss"),
      req.cantidad,
      req.mensaje,
      estado,
    ]);

    const sqlUpdate = "UPDATE Mesa SET estado = 'ocupada' WHERE id_mesa = ?";
    await pool.query(sqlUpdate, [req.id_mesa]);

    const user = rows[0];
    const data = {
      ...user,
      mensaje: "Reserva creada exitosamente",
    };

    res.send({ data });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const patchReserva = async (req, res) => {
  try {
    const { id } = req.params;
    const { id_mesa, estado } = req.body;
    const fechaActual = moment();
    const fecha_hora = req.body.fecha_hora ? moment(req.body.fecha_hora, "DD-MM-YYYY HH:mm") : null;

    if (fecha_hora && fecha_hora.isBefore(fechaActual)) {
      return res.send({
        mensaje: "La reserva no puede ser previo a la fecha y hora actual",
      });
    }

    const sqlGetReserva = "SELECT id_mesa FROM Reserva WHERE id_reserva = ?";
    const [reservaResult] = await pool.query(sqlGetReserva, [id]);

    const data = [id_mesa, fecha_hora ? fecha_hora.format("YYYY-MM-DD HH:mm:ss") : null, estado, id];
    const sql =
      "UPDATE Reserva SET id_mesa = IFNULL(?, id_mesa), fecha_hora = IFNULL(?, fecha_hora), estado = IFNULL(?, estado) WHERE id_reserva = ?";
    const [result] = await pool.query(sql, data);

    if (result.affectedRows === 0) {
      return res.status(404).send({ data: { mensaje: "Reserva no encontrada", statusCode: 404 } });
    }

    const id_mesa_anterior = reservaResult[0].id_mesa;

    if (id_mesa_anterior && id_mesa_anterior !== id_mesa) {
      await pool.query("UPDATE Mesa SET estado = 'disponible' WHERE id_mesa = ?", [id_mesa_anterior]);
    }

    if (id_mesa) {
      await pool.query("UPDATE Mesa SET estado = 'ocupada' WHERE id_mesa = ?", [id_mesa]);
    }

    res.send({
      data: { mensaje: "Reserva actualizada con exito", statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};
export const delReserva = async (req, res) => {
  try {
    const { id } = req.params;

    const sqlGet = "SELECT id_mesa FROM Reserva where id_reserva = ?";
    const [getResult] = await pool.query(sqlGet, id);

    if (getResult.length === 0) {
      return res.status(404).send({ data: { mensaje: "Reserva no encontrada", statusCode: 404 } });
    }

    const id_mesa = getResult[0].id_mesa;

    const sql = "DELETE FROM Reserva WHERE id_reserva = ?";
    const [deleteResult] = await pool.query(sql, id);

    if (deleteResult.affectedRows === 0) {
      return res.status(404).send({ data: { mensaje: "Reserva no encontrada", statusCode: 404 } });
    }

    const sqlUpdate = "UPDATE Mesa SET estado = 'disponible' WHERE id_mesa = ?";
    await pool.query(sqlUpdate, id_mesa);

    res.send({
      data: { mensaje: "Reserva eliminada con exito", statusCode: 200 },
    });
  } catch (error) {
    const err = error.message;
    const statusCode = error.statusCode || 500;
    const data = {
      err,
      mensaje: "Error en el servidor",
      statusCode,
    };
    res.status(500).send({ data });
  }
};

export const cancelReserva = async (req, res) => {
  try {
    const { id } = req.params;

    // Obtener la reserva actual
    const [rows] = await pool.query("SELECT * FROM Reserva WHERE id_reserva = ?", [id]);

    if (rows.length === 0) {
      return res.status(404).send({ mensaje: "Reserva no encontrada" });
    }

    const reserva = rows[0];

    // Actualizar el estado de la reserva a 'cancelada'
    await pool.query("UPDATE Reserva SET estado = 'cancelada' WHERE id_reserva = ?", [id]);

    // Actualizar el estado de la mesa asociada a 'disponible'
    await pool.query("UPDATE Mesa SET estado = 'disponible' WHERE id_mesa = ?", [reserva.id_mesa]);

    res.send({ mensaje: "Reserva cancelada exitosamente" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ mensaje: "Error en el servidor" });
  }
};
export const cancelReservaGeneral = async (req, res) => {
  try {
    const { id } = req.params;

    // Obtener la reserva actual
    const [rows] = await pool.query("SELECT * FROM ReservaGeneral WHERE id_reserva = ?", [id]);

    if (rows.length === 0) {
      return res.status(404).send({ mensaje: "Reserva no encontrada" });
    }

    const reserva = rows[0];

    // Actualizar el estado de la reserva a 'cancelada'
    await pool.query("UPDATE ReservaGeneral SET estado = 'cancelada' WHERE id_reserva = ?", [id]);

    // Actualizar el estado de la mesa asociada a 'disponible'
    await pool.query("UPDATE Mesa SET estado = 'disponible' WHERE id_mesa = ?", [reserva.id_mesa]);

    res.send({ mensaje: "Reserva cancelada exitosamente" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ mensaje: "Error en el servidor" });
  }
};

// Función para liberar las mesas asociadas a reservas
const liberarMesas = async () => {
  try {
    const tiempoLimite = moment().subtract(2, "hours");
    // Consultar las reservas confirmadas que han superado el tiempo límite
    const [rows] = await pool.query(
      "SELECT id_reserva, id_mesa FROM Reserva WHERE estado = 'confirmada' OR estado = 'pendiente' AND fecha_hora <= ?",
      [tiempoLimite.format("YYYY-MM-DD HH:mm:ss")]
    );

    // Liberar las mesas asociadas a las reservas encontradas
    for (const reserva of rows) {
      await pool.query("UPDATE Mesa SET estado = 'disponible' WHERE id_mesa = ?", [reserva.id_mesa]);

      // Actualizar el estado de la reserva a "completada"
      await pool.query("UPDATE Reserva SET estado = 'completada' WHERE id_reserva = ?", [
        reserva.id_reserva,
      ]);
    }

    console.log("Mesas liberadas correctamente");
  } catch (error) {
    console.error("Error al liberar mesas:", error);
  }
};

// Ejecutar la función cada 10 minutos
setInterval(liberarMesas, 10 * 60 * 1000);
