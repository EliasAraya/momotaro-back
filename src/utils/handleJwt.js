import jwt from "jsonwebtoken";
import { pool } from "../db.js";

const jwt_secret = process.env.JWT_KEY;

/**
 * Debes de pasar el objeto del usuario
 */
export const tokenSign = async (usuario) => {
  const sign = jwt.sign(
    {
      email: usuario.email,
      tipo: usuario.tipo,
    },
    jwt_secret,
    {
      expiresIn: "2h",
    }
  );
  return sign;
};

/**
 * Debes de pasar elt oken de session el JWT
 * @returns
 */
export const verifyToken = async (tokenJwt) => {
  try {
    const message = "TOKEN INVALIDO";
    const [rows] = await pool.query(
      "SELECT * FROM Token WHERE black_list = ?",
      [tokenJwt]
    );
    if (rows.length > 0) {
      return message;
    }
    return jwt.verify(tokenJwt, jwt_secret);
  } catch (error) {
    return error;
  }
};
