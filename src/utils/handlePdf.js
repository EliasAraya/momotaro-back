import fs from "fs";
import pdf from "pdfkit";
/* const direccion = "Las higueras 2995,San joaquin, Santiago";
const invoice = {
  usuario: {
    nombre: "Luis",
    direccion: direccion.split(",")[0],
    comuna: direccion.split(",")[1],
    ciudad: direccion.split(",")[2],
  },
  productos: [
    {
      nombre: "pepo",
      descripcion: "DEscripcion",
      precio: 4040,
      cantidad: 55,
      total: 50000,
    },
  ],
  pedido: {
    id_pedido: 1234,
    fecha: "20/5/2023",
  },
  detalle: {
    cantidad: 55,
    total: 5000,
  },
  subtotal: 10000,
}; */

export const createInvoice = async (invoice, path) => {
  let doc = new pdf({ size: "A4", margin: 50 });

  generateHeader(doc);
  generateCustomerInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  generateFooter(doc);

  /* doc.pipe(fs.createWriteStream(path)); */
  return doc;
};

function generateHeader(doc) {
  doc
    .image("src/uploads/momotaro.png", 50, 45, { width: 50 })
    .fillColor("#444444")
    .fontSize(20)
    .text("Momotaro Ramen", 110, 57)
    .fontSize(10)
    .text("Momotaro.", 200, 50, { align: "right" })
    .text("Patio Bellavista", 200, 65, { align: "right" })
    .text("Bellavista 052, Providencia, Santiago", 200, 80, { align: "right" })
    .moveDown();
}

function generateCustomerInformation(doc, invoice) {
  doc.fillColor("#444444").fontSize(20).text("Detalle", 50, 160);

  generateHr(doc, 185);

  const customerInformationTop = 200;

  doc
    .fontSize(10)
    .text("Numero Documento:", 50, customerInformationTop)
    .font("Helvetica-Bold")
    .text(invoice.pedido.id_pedido, 150, customerInformationTop)
    .font("Helvetica")
    .text("Fecha Compra:", 50, customerInformationTop + 15)
    .text(invoice.pedido.fecha, 150, customerInformationTop + 15)
    .text("Total:", 50, customerInformationTop + 30)
    .text(precioFormateado(invoice.subtotal), 150, customerInformationTop + 30)

    .font("Helvetica-Bold")
    .text(invoice.usuario.nombre, 300, customerInformationTop)
    .font("Helvetica")
    .text(invoice.usuario.calle, 300, customerInformationTop + 15)
    .text(invoice.usuario.comuna + ", " + invoice.usuario.ciudad, 300, customerInformationTop + 30)
    .moveDown();

  generateHr(doc, 252);

  /* console.log(
    "CALLE ",
    invoice.usuario.calle,
    " COMUNA ",
    invoice.usuario.comuna,
    " CIUDAD ",
    invoice.usuario.ciudad
  ); */
  console.log("CONJUNTO ", invoice.usuario.comuna + ", " + invoice.usuario.ciudad);
}

function generateInvoiceTable(doc, invoice) {
  const invoiceTableTop = 330;

  doc.font("Helvetica-Bold");
  generateTableRow(doc, invoiceTableTop, "Producto", "Descripcion", "Costo unitario", "Cantidad", "Total");
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");

  invoice.productos.forEach((item, index) => {
    const position = invoiceTableTop + (index + 1) * 30;
    generateTableRow(
      doc,
      position,
      item.nombre,
      item.descripcion,
      precioFormateado(item.precio),
      item.cantidad,
      precioFormateado(item.total)
    );

    generateHr(doc, position + 20);
  });

  const subtotalPosition = invoiceTableTop + (invoice.productos.length + 1) * 30;
  generateTableRow(
    doc,
    subtotalPosition,
    "",
    "",
    "Subtotal",
    "",
    precioFormateado(invoice.subtotal * 0.81)
  );

  const paidToDatePosition = subtotalPosition + 20;
  generateTableRow(
    doc,
    paidToDatePosition,
    "",
    "",
    "Iva 19%",
    "",
    precioFormateado(invoice.subtotal * 0.19)
  );

  const duePosition = paidToDatePosition + 25;
  doc.font("Helvetica-Bold");
  generateTableRow(doc, duePosition, "", "", "Total a pagar", "", precioFormateado(invoice.subtotal));
  console.log("Precio a pagar ", invoice.subtotal);
  doc.font("Helvetica");
}

function generateFooter(doc) {
  doc.fontSize(10).text("Gracias por su preferencia.", 50, 780, {
    align: "center",
    width: 500,
  });
}

function generateTableRow(doc, y, nombre, descripcion, costoUnidad, cantidad, total) {
  doc
    .fontSize(10)
    .text(nombre, 50, y)
    .text(descripcion, 150, y)
    .text(costoUnidad, 280, y, { width: 90, align: "right" })
    .text(cantidad, 370, y, { width: 90, align: "right" })
    .text(total, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc.strokeColor("#aaaaaa").lineWidth(1).moveTo(50, y).lineTo(550, y).stroke();
}

function precioFormateado(cent) {
  return new Intl.NumberFormat("es-CL", {
    style: "currency",
    currency: "CLP",
  }).format(cent);
}
