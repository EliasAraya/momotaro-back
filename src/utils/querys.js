/**
 * ---- SQL PEDIDOS ----
 */

/**
 * Controller Obtener pedido/s
 */
export const sqlGetPedido = "SELECT * FROM Pedido WHERE id_usuario = ?";
export const sqlGetAllPedido = "SELECT * FROM Pedido ";
/**
 * Controller Cancelar Pedido
 */
export const sqlPutPedido =
  "UPDATE Pedido SET estado = 'cancelado' WHERE id_pedido = ? AND NOT estado = 'cancelado'";
export const sqlDetallePedido = "SELECT id_producto, cantidad FROM DetallePedido WHERE id_pedido = ?";
export const sqlStockPedido = "SELECT stock FROM Producto WHERE id_producto = ?";
export const sqlUpdateStock = "UPDATE Producto SET stock = ? WHERE id_producto = ?";

/**
 * Controller Crear Pedido
 */
export const sqlPostPedido = "INSERT INTO Pedido (id_usuario, fecha, estado) VALUES (?,?,'en proceso')";
export const sqlVerificarStock =
  "SELECT nombre, precio, stock FROM Producto WHERE id_producto = ? AND stock >= ?";
export const sqlUpdateStockPost = "UPDATE Producto SET stock = ? WHERE id_producto = ?";
export const sqlPostDetalle =
  "INSERT INTO DetallePedido (id_pedido,id_producto,cantidad,total) VALUES (?,?,?,?)";

/**
 * Controller Imprimir Factura
 */
export const sqlFactura = `SELECT 
d.id_pedido 
,d.id_producto
,d.cantidad
,d.total
,p2.id_usuario
,p2.fecha
,p.nombre
,p.descripcion
,p.precio
,u.nombre_usuario,
u.direccion
FROM 
DetallePedido d 
INNER JOIN Producto p ON d.id_producto = p.id_producto 
INNER JOIN Pedido p2 ON d.id_pedido = p2.id_pedido 
INNER JOIN Usuario u ON p2.id_usuario = u.id_usuario 
WHERE 
p2.id_pedido = ?`;
