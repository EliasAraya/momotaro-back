import bcryptjs from "bcrypt";

export const encrypt = async (passwordPlaint) => {
  const hash = await bcryptjs.hash(passwordPlaint, 10);
  return hash;
};

export const compare = async (passwordPlaint, hashPassword) => {
  return await bcryptjs.compare(passwordPlaint, hashPassword);
};
