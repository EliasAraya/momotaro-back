import express from "express";
import "dotenv/config.js";
import authRoutes from "./src/routes/auth.routes.js";
import reservasRoutes from "./src/routes/reserva.routes.js";
import pedidosRoutes from "./src/routes/pedido.routes.js";
import usersRoutes from "./src/routes/users.routes.js";
import mesaRoutes from "./src/routes/mesa.routes.js";
import productoRoutes from "./src/routes/producto.routes.js";
import cors from "cors";

const app = express();
app.use(cors());
app.use(express.json());

app.use("/api", authRoutes);
app.use("/api", reservasRoutes);
app.use("/api", pedidosRoutes);
app.use("/api", usersRoutes);
app.use("/api", mesaRoutes);
app.use("/api", productoRoutes);

app.listen(3000);
